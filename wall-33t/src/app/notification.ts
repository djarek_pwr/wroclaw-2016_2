import {Component, Input} from 'angular2/core';

@Component({
    selector: 'notification',

    template: `
        <div [hidden]="text!='Mariusz'" class="notification alert alert-info">Mariusz is lecturing in C025</div>
        <div [hidden]="text=='Mariusz' || text!=pattern" class="notification alert alert-success">{{notification}}</div>
`
})

export class Notification {
    @Input() text:string;
    @Input() pattern:string;
    @Input() notification:string;
}
