import {Component} from 'angular2/core';
import {Notification} from './notification';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
    selector: 'walleet',
    directives: [Notification],
    providers: [Cookie],
    template: `
        <form>
            <input type="text" [(ngModel)]="fileName" placeholder="Label">
            <input type="text" [(ngModel)]="fileHash" placeholder="Hash">
            <button type="submit" (click)="onSubmitClick()">Store</button>
        </form>

        <div class="row file-list">

            <div class="col-xs-6 file-list-title">Label</div>
            <div class="col-xs-6 file-list-title">Hash</div>
            <div class="row"*ngFor="#ha of keys()">
            <div  class="col-xs-6">{{ha}}</div>
            <div  class="col-xs-6">{{hashes[ha]}}</div>
            </div>
            <!-- <div class="col-xs-6">Element 1</div>
            <div class="col-xs-6">8743b52063cd84097a65d1633f5c74f5</div>
            <div class="col-xs-6">Element 1</div>
            <div class="col-xs-6">8743b52063cd84097a65d1633f5c74f5</div>
            <div class="col-xs-6">Element 2</div>
            <div class="col-xs-6">bfd280436f45fa38eaacac3b00518f29</div>
            <div class="col-xs-6">Element 1</div>
            <div class="col-xs-6">8743b52063cd84097a65d1633f5c74f5</div>
            <div class="col-xs-6">Element 2</div>
            <div class="col-xs-6">bfd280436f45fa38eaacac3b00518f29</div>
            <div class="col-xs-6">Element 1</div>
            <div class="col-xs-6">8743b52063cd84097a65d1633f5c74f5</div>
            <div class="col-xs-6">Element 2</div>
            <div class="col-xs-6">bfd280436f45fa38eaacac3b00518f29</div> -->
        </div>
`
})

export class Walleet {
    yourName:string = '';
    pattern:string = '';
    notification:string = '';
    KEY:string = 'HASHES_STORAGE_KEY';
    fileName:string = '';
    fileHash:string = '';
    hashes:Dictionary = {'Element 1':'bfd280436f45fa38eaacac3b00518f29',
                         'Element 2':'8743b52063cd84097a65d1633f5c74f5'};

    constructor() {
      this.hashes = <Dictionary>JSON.parse(Cookie.get(this.KEY));
    }
    onSubmitClick() {
      this.hashes[this.fileName] = this.fileHash;
      Cookie.set(this.KEY, JSON.stringify(this.hashes));
    }
    keys() : Array<string> {
      return Object.keys(this.hashes);
    }
}
interface Dictionary {
    [ index: string ]: string;
}

//<notification [text]="yourName" [pattern]="pattern" [notification]="notification"></notification>
//    <label>Hello, whats your name?</label>
//<input type="text" [(ngModel)]="yourName" placeholder="Enter a name here">
//    <h1 [hidden]="!yourName"><small>Nice to meet you,</small> {{yourName}}!</h1>
//
//<label>Notification Maker 3000</label>
//<input type="text" [(ngModel)]="pattern" placeholder="Enter a pattern...">
//<input type="text" [(ngModel)]="notification" placeholder="and the notification">
