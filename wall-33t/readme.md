Squad of the group: 
Damian Jarek
Michał Kiewra
Aleksandra Płaskowicka
Adam Żółtowski

The app could be modified in very simple way. The whole storage of hashes could be in one file which will be deployed on IPFS. 
The hash of this file would be stored in memory of the browser (cookies) or inserted by the user to get acces from different storage.
The modification of the application which is required is to store data instad of in browser files in separate file.
This file would be Added to IPFS and the hash will be generated. 
This work could be done by application so it could store hash of data for compfort of the user.
It is required to add possibility to the app to insert hash to import data from IPFS.
In order to enable the user to modify the hash-base on different devices the application could make use of IPNS, which is a namespace
solution for IPFS which allows assigning a resource hash to a namespace (simillar to URLs in HTTP).
