import {Component} from 'angular2/core';
import {Notification} from './notification';

@Component({
    selector: 'hello-world',
    directives: [Notification],

    template: `
    <notification [text]="yourName" [pattern]="pattern" [notification]="notification"></notification>
  	<label>Hello, whats your name?</label>
	<input type="text" [(ngModel)]="yourName" placeholder="Enter a name here">
	<h1 [hidden]="!yourName"><small>Nice to meet you,</small> {{yourName}}!</h1>

	<label>Notification Maker 3000</label>
	<input type="text" [(ngModel)]="pattern" placeholder="Enter a pattern...">
	<input type="text" [(ngModel)]="notification" placeholder="and the notification">
`
})

export class HelloWorld {
    yourName:string = '';
    pattern:string = '';
    notification:string = '';
}
